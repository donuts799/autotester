#include "AutoTester.h"


AutoTester::AutoTester(std::list<std::string> projects, std::list<ScriptInfo> scripts, AutoTesterSettings settings)
{
	this->projectList	= projects;

	this->scripts		= scripts;

	this->settings		= settings;
}


AutoTester::~AutoTester()
{
}

void AutoTester::run()
{
	for(std::string projectName : projectList)
	{
		runProject(projectName);
	}
}

/*
	Starts running project by waiting until a thread is available, and then
	starting the first script. If multithreading scripts is on, it will kick
	off each script as threads are available, otherwise it will return after
	starting the first script.
*/
void AutoTester::runProject(std::string projectName)
{
	conditionMutex.lock();
	testerRunning = true;
	conditionMutex.unlock();

	if (settings.scriptsMultithreaded) // allocate a new thread for each script
	{
		for (auto scriptInfo : scripts)
		{
			{
				std::unique_lock<std::mutex> lock(conditionMutex);			// lock access to the thread count variables
				while (allocatedThreadCount >= settings.MaxThreadCount)		// if we are at our max, wait until a thread is free
				{
					condition.wait(lock);
				}
				// We have an available thread at this point

				allocatedThreadCount++;
			}

			std::thread scriptThread = std::thread(&AutoTester::runScript, this, projectName, scriptInfo);
		}

	}
	else // allocate one thread for all of the scripts
	{
		{
			std::unique_lock<std::mutex> lock(conditionMutex);			// lock access to the thread count variables
			while (allocatedThreadCount >= settings.MaxThreadCount)		// if we are at our max, wait until a thread is free
			{
				condition.wait(lock);
			}
			// We have an available thread at this point

			allocatedThreadCount++;
		}

		std::thread scriptsThread = std::thread(&AutoTester::runScripts, this, projectName);
	}
}

void AutoTester::runScript(std::string projectName, ScriptInfo scriptInfo)
{
	timeScript(projectName, scriptInfo);


	// change count to reflect a thread has been terminated, and signal
	conditionMutex.lock();
	allocatedThreadCount--;
	if (allocatedThreadCount == 0) testerRunning = false; // we have now finished testing
	conditionMutex.unlock();
	condition.notify_all();
}

void AutoTester::runScripts(std::string projectName)
{
	for (auto scriptInfo : scripts)
	{
		timeScript(projectName, scriptInfo);
	}


	// change count to reflect a thread has been terminated, and signal
	conditionMutex.lock();
	allocatedThreadCount--;
	if (allocatedThreadCount == 0) testerRunning = false; // we have now finished testing;
	conditionMutex.unlock();
	condition.notify_all();
}

void AutoTester::timeScript(std::string projectName, ScriptInfo scriptInfo)
{
	// time how long the script took
	time_t start = time(0);

	std::string result = ExecAndProcessOutput(projectName, scriptInfo.script, scriptInfo.timeout);

	time_t end = time(0);

	// process the results
	conditionMutex.lock();



	conditionMutex.unlock();
}

void AutoTester::waitUntilFinished()
{
	std::unique_lock<std::mutex> lock(conditionMutex);
	while (testerRunning)
	{
		condition.wait(lock);
	}
}
