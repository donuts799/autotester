#include "Json_ToStringCharacteristics.h"


Json_ToStringCharacteristics Json_ToStringCharacteristics::next()
{
	Json_ToStringCharacteristics next = *this;
	next.indentCount++;
	return next;
}
