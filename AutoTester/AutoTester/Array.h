#pragma once

/*
	Array.h
		BrendanLib
			class Array<T>

	This file contains the Array<T> class. This file is designed for using a C++
	array safely. It implements an iterator for ease-of-use, and bounds checking.
*/


// no includes were necessary since this is a very basic implementation of an Array

//TODO: figure out Iterator lifespan (when Array<T> is deleted)


namespace BrendanLib
{
	typedef size_t ArrayIndex;// used to index the Array

	/*
		Array<T>

		Class for using an array with bounds checking, lifetime management, and "Exception"s
	*/
	template<typename StorageType>
	class Array
	{
	 protected:
		ArrayIndex length = 0;			// number of items that are stored in the "rawArray"
		
		bool deleteArrayOnDestruction = true;// if array was given, we might not want to delete it

	public:

		/*
			Iterator

			Class to iterate through the Array<T>
		*/
		class Iterator
		{
		protected:
			const Array<StorageType>& arrayReference;	// reference to the array to iterate through
			ArrayIndex currentIndex = 0;				// current index that is referenced within 
														// -the Array

		public:
			friend class Array<StorageType>;			// created as a friend of the array to allow
														// -access to raw data instead of going
														// -through public functions

			/*
				Iterator(const Array<StorageType>&, ArrayIndex)
				
				@param data:			the array to iterate through
				@param initialIndex:	the index for this Iterator to start at

				Constructor that specifies the Array, and where this Iterator should start within 
				the array
			*/
			Iterator(const Array<StorageType>& data, ArrayIndex initialIndex = 0);

			/*
				operator++()

				Increments the index in the Array that this Iterator refers to
			*/
			Iterator& operator++();

			/*
				operator*()

				Gives access to the data stored at the index referenced
			*/
			StorageType& operator*() const;

			/*
				operator!=(const Iterator&)

				@param other: the other Iterator to compare to

				Returns true if "this" Iterator, and the "other" Iterator don't reference the same
				index, otherwise returns false.
			*/
			bool operator!=(const Iterator& other) const;
		};
		
		/*
			Array(ArrayIndex)

			@param initialSize: used to decide the initial amount to allocate for the array

			Constructor that specifies the initial size of the Array
		*/
		Array(ArrayIndex initialSize = 0);

		/*
			Array(StorageType*, ArrayIndex)

			@param data: the array of data to create an Array wrapper around
			@param size: the size of the array provided
			@param copy: whether or not to copy from the array to a new one, or use the old
			@param deleteOnDestruction: whether to delete the raw array, when the destructor is called

			Constructor that takes an existing array, and wraps the Array functionality around it.
			Can copy the old array to a newly allocated one if desired.
			Can optionally delete the provided array if specified.
		*/
		Array(StorageType* data, ArrayIndex size, bool copy = false, bool deleteOnDestruction = true);

		/*
			Array(const Array<StorageType>&)

			@param other: the "other" Array to copy from

			Copy constructor
		*/
		Array(const Array<StorageType>& other);

		/*
			Array(Array<StorageType>&&)

			@param other: the "other" Array to move from

			Move constructor
		*/
		Array(Array<StorageType>&& other);

		/*
			operator=(Array<StorageType>)

			@param other: the "other" Array to copy from

			Copy assignment operator
		*/
		Array<StorageType>& operator=(Array<StorageType> other);

		/*
			swap(Array<StorageType>&, Array<StorageType>&)

			@param first:	the first Array to swap
			@param second:	the second Array to swap

			Swaps the contents of the two objects specified. Used for the Copy-Swap idiom
		*/
		friend void swap(Array<StorageType>& first, Array<StorageType>& second)
		{
			// this function is inlined in the declaration per this article
			// https://web.mst.edu/~nmjxv3/articles/templates.html

			//using std::swap;TODO:

			std::swap(first.length, second.length);
			std::swap(first.rawArray, second.rawArray);
			std::swap(first.deleteArrayOnDestruction, second.deleteArrayOnDestruction);
		}

		/*
			~Array()

			Destructor deletes the array if one had been allocated
		*/
		~Array();

		/*
			size()

			This function returns the number of elemelts in the array
		*/
		ArrayIndex size() const;

		/*
			resize(newSize)

			@param newSize: the number of elements the array should have

			This function allocates a new array, and copies any old elements up to the newSize or
			oldSize (whichever is smaller), and then sets "this" array to the new one and deletes
			the old array.
		*/
		void resize(ArrayIndex newSize);

		/*
			operator[](ArrayIndex)

			@param index: the index in the array to retrieve the StorageType reference from

			Retrieve the reference to the element in the array at the specified index
		*/
		StorageType& operator[](ArrayIndex index) const;

		/*
			begin()

			Returns an Iterator that starts at the beginning of the Array
		*/
		Iterator begin() const 
		{
			return Iterator(*this, 0);
		}

		/*
			end()

			Returns an Iterator that starts at the end of the Array
		*/
		Iterator end() const 
		{
			return Iterator(*this, length);
		}

		StorageType* rawArray = NULL;	// raw data that is stored
	};

	//Array<StorageType> function definitions

	template<typename StorageType>
	inline Array<StorageType>::Array(ArrayIndex initialSize)
	{
		if(initialSize)
			rawArray = new StorageType[initialSize];

		length = initialSize;
	}

	template<typename StorageType>
	inline Array<StorageType>::Array(StorageType* data, ArrayIndex size, bool copy, bool deleteOnDestruction)
	{
		deleteArrayOnDestruction = deleteOnDestruction;
		if (copy)	// copy the contents
		{
			//---allocate new array---//
			rawArray	= new StorageType[size];
			length		= size;

			//---transfer from old, to new---//
			for (ArrayIndex i = 0; i < size; i++)
			{
				rawArray[i] = data[i];
			}
		}
		else		// use the existing array, and wrap this "Array" functionality around it
		{
			rawArray	= data;
			length		= size;
		}
	}

	template<typename StorageType>
	inline Array<StorageType>::Array(const Array<StorageType>& other)
	{
		if (other.length != 0)
		{
			rawArray = new StorageType[other.length];

			for (ArrayIndex i = 0; i < other.length; i++)
				rawArray[i] = other[i];
		}
		length = other.length;
		deleteArrayOnDestruction = other.deleteArrayOnDestruction;
	}

	template<typename StorageType>
	inline Array<StorageType>::Array(Array<StorageType>&& other)
	{
		swap(*this, other);
	}

	template<typename StorageType>
	inline Array<StorageType>& Array<StorageType>::operator=(Array<StorageType> other)
	{
		swap(*this, other);// copy-swap idiom
		return *this;
	}

	template<typename StorageType>
	inline Array<StorageType>::~Array()
	{
		if (deleteArrayOnDestruction && rawArray && length != 0)
			delete[] rawArray;
	}

	template<typename StorageType>
	inline ArrayIndex Array<StorageType>::size() const
	{
		return length;
	}

	template<typename StorageType>
	inline void Array<StorageType>::resize(ArrayIndex newSize)
	{
		if (newSize == 0)
		{
			if (rawArray)
				delete[] rawArray;
			
			rawArray = nullptr;// so we don't try to delete already-deleted array in destructor
		}
		else if (newSize < length)
		{
			StorageType* newArray = new StorageType[newSize];

			for (int i = 0; i < newSize; i++)
				newArray[i] = std::move(rawArray[i]);

			delete[] rawArray;
			rawArray = newArray;
		}
		else // newSize >= length
		{
			StorageType* newArray = new StorageType[newSize];

			for (int i = 0; i < length; i++)
				newArray[i] = std::move(rawArray[i]);

			delete[] rawArray;
			rawArray = newArray;
		}

		length = newSize;
	}

	template<typename StorageType>
	inline StorageType& Array<StorageType>::operator[](ArrayIndex index) const
	{
		if (index < length)
			return rawArray[index];
		else
			throw IndexOutOfBoundsException(index, __func__, __LINE__, __FILE__);
	}


	// Iterator function definitions

	template<typename StorageType>
	Array<StorageType>::Iterator::Iterator(const Array<StorageType>& data, ArrayIndex initialIndex)
		: arrayReference(data)
	{
		currentIndex = initialIndex;
	}

	template<typename StorageType>
	inline typename Array<StorageType>::Iterator& Array<StorageType>::Iterator::operator++()
	{
		currentIndex++;
		return *this;
	}

	template<typename StorageType>
	inline StorageType& Array<StorageType>::Iterator::operator*() const
	{
		return arrayReference[currentIndex];// return the StorageType& that this iterator is 
											// -currently referencing
	}

	template<typename StorageType>
	inline bool Array<StorageType>::Iterator::operator!=(const Iterator& other) const
	{
		return currentIndex != other.currentIndex;
	}
}

