#pragma once
#include <mutex>
/*
	SafeLock.h
		BrendanLib
			SafeLock
*/



/*
	SafeLock

	SafeLock is a very simple class that allows the user to lock and unlock the item that is passed
	to the constructor safely. If an exception occurs, C++ automatically calls the destructor, which
	unlocks the item.
*/
namespace BrendanLib
{
	template <typename LockType = std::mutex>
	class SafeLock
	{
	private:
		LockType & reference;// reference to the lockable item
	public:

		/*
		SafeLock(LockType&)

		@param lock: item to lock

		Create an object and lock the reference
		*/
		SafeLock(LockType& lock);

		SafeLock(const SafeLock& other) = delete;	// delete the copy constructor 

		SafeLock(SafeLock&& other) = delete;	// delete the move constructor

		/*
		~SafeLock()

		Destructor unlocks the reference
		*/
		~SafeLock();
	};


	template<typename LockType>
	SafeLock<LockType>::SafeLock(LockType& lock) : reference(lock)
	{
		reference.lock();
	}

	template<typename LockType>
	SafeLock<LockType>::~SafeLock()
	{
		reference.unlock();
	}
}