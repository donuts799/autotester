#include "Json.h"

namespace BrendanLib
{

	//Create necessary Exceptions for Json specific errors
	class Json_ParseStringInvalidFormatException : public Exception
	{
	public:
		const JsonStringIndex issueIndex;
		Json_ParseStringInvalidFormatException(JsonStringIndex index, string callingFunction, size_t callingLine, string callingFile) : issueIndex(index)
		{
			severity		= "Error";
			type			= "Json_ParseStringInvalidFormatException";
			errorMessage	= callingFunction + " found an unexpected character at [" + std::to_string(index) + "] while parsing the string.";
			function		= callingFunction;
			line			= callingLine;
			file			= callingFile;
			fix				= "Make sure the string is formatted for JSON.";
		}
	};

	class Json_InvalidTypeException : public Exception
	{
	public:
		Json_InvalidTypeException(string badType, string expectedType, string operation, string callingFunction, size_t callingLine, string callingFile)
		{
			severity		= "Error";
			type			= "Json_InvalidTypeException";
			errorMessage	= "The operation " + operation + " requires the type(s) " + expectedType + " but instead was given a value of type " + badType;
			function		= callingFunction;
			line			= callingLine;
			file			= callingFile;
			fix				= "Verify the type of the object before attempting a type specific operation.";
		}
	};

	class Json_ParseStringOverrunException : public Exception
	{
	public:
		Json_ParseStringOverrunException(string callingFunction, size_t callingLine, string callingFile)
		{
			severity		= "Error";
			type			= "Json_ParseStringOverrunException";
			errorMessage	= "Parsing could not complete, since the string buffer to parse was overrun";
			function		= callingFunction;
			line			= callingLine;
			file			= callingFile;
			fix				= "Make sure the input string is in the correct format. It may have been missing a few characters";
		}
	};


	Json::Json()
	{
		valuePtr = new JsonNull();
	}

	Json::Json(bool initialValue)
	{
		valuePtr = new JsonBool(initialValue);
	}

	Json::Json(double initialValue)
	{
		valuePtr = new JsonNumber(initialValue);
	}

	Json::Json(string initialValue)
	{
		valuePtr = new JsonString(initialValue);
	}

	Json::Json(Array<Json> jsonArray)
	{
		valuePtr = new JsonArray(jsonArray);
	}

	Json::Json(const Json& other)// : Json()
	{
		if (other.valuePtr)
			valuePtr = other.valuePtr->clone();
	}

	Json::Json(Json&& other) : Json()
	{
		swap(*this, other);
	}

	Json& Json::operator=(Json other)
	{
		swap(*this, other);
		return *this;
	}

	Json::~Json()
	{
		if (valuePtr) { delete valuePtr; }//delete resource if allocated
	}

	Json& Json::operator=(bool toSet)
	{
		this->testTypeAndSet(JSON_TYPE::BOOL);
		(*reinterpret_cast<JsonBool*>(this->valuePtr)).value = toSet;
		return *this;
	}

	Json& Json::operator=(const char* const toSet)
	{
		this->testTypeAndSet(JSON_TYPE::STRING);
		(*reinterpret_cast<JsonString*>(this->valuePtr)).value = toSet;
		return *this;
	}

	Json& Json::operator=(double toSet)
	{
		this->testTypeAndSet(JSON_TYPE::NUMBER);
		(*reinterpret_cast<JsonNumber*>(this->valuePtr)).value = toSet;
		return *this;
	}

	Json& Json::operator=(string toSet)
	{
		this->testTypeAndSet(JSON_TYPE::STRING);
		(*reinterpret_cast<JsonString*>(this->valuePtr)).value = toSet;
		return *this;
	}

	JSON_TYPE Json::getType() const
	{
		return valuePtr ? valuePtr->getType() : JSON_TYPE::NIL; //the type of this object.
	}

	string Json::getTypeString(JSON_TYPE type)
	{
		switch (type)
		{
			case JSON_TYPE::NIL:
			{
				return "NIL";
			}
			case JSON_TYPE::BOOL:
			{
				return "BOOL";
			}
			case JSON_TYPE::NUMBER:
			{
				return "NUMBER";
			}
			case JSON_TYPE::STRING:
			{
				return "STRING";
			}
			case JSON_TYPE::ARRAY:
			{
				return "ARRAY";
			}
			case JSON_TYPE::OBJECT_PAIR:
			{
				return "OBJECT_PAIR";
			}
			case JSON_TYPE::OBJECT:
			{
				return "OBJECT";
			}
			default:
			{
				throw Json_InvalidTypeException("Unknown", "Any", "getType", __func__, __LINE__, __FILE__);
			}
		}
	}

	Json Json::parse(string toParse, JsonStringIndex& currentIndex)
	{
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		skipWhiteSpace(toParse, currentIndex);

		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		Json object;
		delete object.valuePtr;// delete JsonNull value that is created by default
		object.valuePtr = NULL;// in case parse throws an error this will make sure a deleted valuePtr doesn't get deleted again
		object.valuePtr = JsonInternalValue::parse(toParse, currentIndex);
		return object;
	}

	Json Json::parse(string toParse)
	{
		JsonStringIndex index = 0;
		return parse(toParse, index);
	}

	Json Json::parse(std::istream& stream)
	{
		JsonStringIndex index = 0;
		std::string fileInput = std::string(std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>());
		return parse(fileInput, index);
	}

	Json::operator double()
	{
		switch (getType())
		{
			case JSON_TYPE::NIL:
			{
				return 0;
			}
			case JSON_TYPE::NUMBER:
			{
				return (*reinterpret_cast<JsonNumber*>(this->valuePtr)).value;//let JsonNumber stringify the number
			}
			case JSON_TYPE::ARRAY:
			{
				throw Json_InvalidTypeException(getTypeString(getType()), getTypeString(JSON_TYPE::NUMBER), "doubleConversion", __func__, __LINE__, __FILE__);
			}
			case JSON_TYPE::OBJECT:
			{
				throw Json_InvalidTypeException(getTypeString(getType()), getTypeString(JSON_TYPE::NUMBER), "doubleConversion", __func__, __LINE__, __FILE__);
			}
			case JSON_TYPE::OBJECT_PAIR:// won't actually be used, but is present for the interface
			{
				throw Json_InvalidTypeException(getTypeString(getType()), getTypeString(JSON_TYPE::NUMBER), "doubleConversion", __func__, __LINE__, __FILE__);
			}
			case JSON_TYPE::STRING:
			{
				throw Json_InvalidTypeException(getTypeString(getType()), getTypeString(JSON_TYPE::NUMBER), "doubleConversion", __func__, __LINE__, __FILE__);
			}
			case JSON_TYPE::BOOL:
			{
				return (*reinterpret_cast<JsonBool*>(this->valuePtr)).value;
			}
			default:
			{
				throw Json_InvalidTypeException(getTypeString(getType()), getTypeString(JSON_TYPE::NUMBER), "doubleConversion", __func__, __LINE__, __FILE__);
			}
		}
	}

	Json::operator string()
	{
		switch (getType())
		{
			case JSON_TYPE::NIL:
			{
				return "";
			}
			case JSON_TYPE::NUMBER:
			{
				return valuePtr->serialize();//let JsonNumber stringify the number
			}
			case JSON_TYPE::ARRAY:
			{
				return valuePtr->serialize();
			}
			case JSON_TYPE::OBJECT:
			{
				return valuePtr->serialize();
			}
			case JSON_TYPE::OBJECT_PAIR:// won't actually be used, but is present for the interface
			{
				throw Json_InvalidTypeException(getTypeString(getType()), getTypeString(JSON_TYPE::STRING), "stringConversion", __func__, __LINE__, __FILE__);
			}
			case JSON_TYPE::STRING:
			{
				return (*reinterpret_cast<JsonString*>(this->valuePtr)).value;
			}
			case JSON_TYPE::BOOL:
			{
				return valuePtr->serialize();
			}
			default:
			{
				throw Json_InvalidTypeException(getTypeString(getType()), getTypeString(JSON_TYPE::STRING), "stringConversion", __func__, __LINE__, __FILE__);
			}
		}
	}

	Json::operator bool()
	{
		switch (getType())
		{
			case JSON_TYPE::NIL:
			{
				return false;
			}
			case JSON_TYPE::NUMBER:
			{
				return (*reinterpret_cast<JsonNumber*>(this->valuePtr)).value != 0;
			}
			case JSON_TYPE::ARRAY:
			{
				return size() != 0;
			}
			case JSON_TYPE::OBJECT:
			{
				return size() != 0;
			}
			case JSON_TYPE::OBJECT_PAIR:// won't actually be used, but is present for the interface
			{
				throw Json_InvalidTypeException(getTypeString(getType()), getTypeString(JSON_TYPE::BOOL), "boolConversion", __func__, __LINE__, __FILE__);
			}
			case JSON_TYPE::STRING:
			{
				return (*reinterpret_cast<JsonString*>(this->valuePtr)).value != "";
			}
			case JSON_TYPE::BOOL:
			{
				return (*reinterpret_cast<JsonBool*>(this->valuePtr)).value;
			}
			default:
			{
				return false;
			}
		}
	}

	Json& Json::operator[](JsonArrayIndex index)
	{
		testTypeAndSet(JSON_TYPE::ARRAY);
		JsonArrayIndex neededSize = index + 1;

		if ((*reinterpret_cast<JsonArray*>(this->valuePtr)).value.size() <= index)
			(*reinterpret_cast<JsonArray*>(this->valuePtr)).value.resize(neededSize);

		return (*reinterpret_cast<JsonArray*>(this->valuePtr)).value[index];
	}

	JsonArrayIndex Json::size()
	{
		if (getType() == JSON_TYPE::ARRAY)
			return (*reinterpret_cast<JsonArray*>(this->valuePtr)).value.size();
		else if (getType() == JSON_TYPE::OBJECT)
			return (*reinterpret_cast<JsonObject*>(this->valuePtr)).value.size();
		else if (getType() == JSON_TYPE::NIL)
			return 0;

		// implied else
		return 1;
	}

	Json& Json::operator[](string member)
	{
		testTypeAndSet(JSON_TYPE::OBJECT);
		JsonObjectPair* testFor = new JsonObjectPair();
		testFor->name.value = member;
		auto& set = (*reinterpret_cast<JsonObject*>(this->valuePtr)).value;
		auto item = set.find(testFor);

		if (item == set.end())// value isn't in set
		{
			// create new value and insert into set

			set.insert(testFor);	// insert NULL value;
			return testFor->value;	// return blank Json object that was just inserted
		}
		else// value is in the set
		{
			delete testFor;//wasn't needed
			return (*item)->value;	// return the value at the specified location
		}
	}

	string Json::serialize(Json_ToStringCharacteristics characteristics) const
	{
		return valuePtr->serialize(characteristics);
	}

	bool Json::contains(Json& contained, bool verifyValues, bool verifyStructure)
	{
		switch (this->getType())
		{
			case JSON_TYPE::NIL:
			{
				if (contained.getType() == JSON_TYPE::NIL)
					return true;
				else
					return false;
			}
			case JSON_TYPE::NUMBER:
			{
				if (contained.getType() == JSON_TYPE::NUMBER)
				{
					if (verifyValues)
						return ((JsonNumber*)valuePtr)->value == ((JsonNumber*)contained.valuePtr)->value;
					else// no need to compare values, so we are already equivalent
						return true;
				}
				else
					return false;
			}
			case JSON_TYPE::ARRAY:
			{
				if (contained.getType() == JSON_TYPE::ARRAY)
				{
					if(verifyValues || verifyStructure)
					{
						if (((JsonArray*)this->valuePtr)->value.size() >= ((JsonArray*)contained.valuePtr)->value.size())
						{
							for (JsonArrayIndex i = 0; i < ((JsonArray*)contained.valuePtr)->value.size(); i++)
							{
								bool contains = ((JsonArray*)this->valuePtr)->value[i].contains(((JsonArray*)contained.valuePtr)->value[i], verifyValues, verifyStructure);
								if(!contains)
								{
									return false;// return false at first non-contained value
								}
							}
							return true;
						}
						else// if "this" is smaller, we can't contain "contained"
							return false;
					}
					else
						return true;
				}
				else
					return false;
			}
			case JSON_TYPE::OBJECT:
			{
				if (contained.getType() == JSON_TYPE::OBJECT)
				{
					if (verifyValues || verifyStructure)
					{
						//--verify each pair---//
						for (JsonObjectPair* otherPair : ((JsonObject*)contained.valuePtr)->value)
						{
							auto found = ((JsonObject*)valuePtr)->value.find(otherPair);
							if (found != ((JsonObject*)valuePtr)->value.end())
							{
								// pair is in "this" set, verify value
								if (!(*found)->value.contains(otherPair->value, verifyValues, verifyStructure))
								{
									return false;// return false at first non-contained value
								}
							}
							else // not in set
							{
								return false;
							}
						}
						return true;
					}
					else
						return true;
				}
				else
					return false;
			}
			case JSON_TYPE::STRING:
			{
				if (contained.getType() == JSON_TYPE::STRING)
				{
					if (verifyValues)
						return ((JsonString*)valuePtr)->value == ((JsonString*)contained.valuePtr)->value;
					else// no need to compare values, so we are already equivalent
						return true;
				}
				else
					return false;
			}
			case JSON_TYPE::BOOL:
			{
				if (contained.getType() == JSON_TYPE::BOOL)
				{
					if (verifyValues)
						return ((JsonBool*)valuePtr)->value == ((JsonBool*)contained.valuePtr)->value;
					else// no need to compare values, so we are already equivalent
						return true;
				}
				else
					return false;
			}
			default:
			{
				throw Json_InvalidTypeException(getTypeString(getType()), "ANY", "compare", __func__, __LINE__, __FILE__);
			}
		}
	}

	void Json::skipWhiteSpace(string toParse, JsonStringIndex& currentIndex)
	{
		while (currentIndex < toParse.length())
		{
			if (std::isspace(toParse[currentIndex]))
			{
				currentIndex++;
				continue;
			}
			// implied else
			break;
		}
	}

	void Json::testTypeAndSet(JSON_TYPE toSet)
	{
		if (!valuePtr || (getType() != toSet))//if valuePtr hasn't been set or is not correct
		{
			if (valuePtr)
				delete valuePtr;//delete old type

			switch (toSet)
			{
				case JSON_TYPE::NIL:
				{
					valuePtr = new JsonNull();
					break;
				}
				case JSON_TYPE::NUMBER:
				{
					valuePtr = new JsonNumber(0);
					break;
				}
				case JSON_TYPE::ARRAY:
				{
					valuePtr = new JsonArray(Array<Json>());
					break;
				}
				case JSON_TYPE::OBJECT:
				{
					valuePtr = new JsonObject();
					break;
				}
				case JSON_TYPE::STRING:
				{
					valuePtr = new JsonString("");
					break;
				}
				case JSON_TYPE::BOOL:
				{
					valuePtr = new JsonBool(false);
					break;
				}
				default:
				{
					throw Json_InvalidTypeException(getTypeString(getType()), "ANY", "compare", __func__, __LINE__, __FILE__);
				}
			}
		}
	}

	/*string JsonInt::toString(ToStringCharacteristics characteristics)
	{
	return string();
	}*/

	JsonNumber::JsonNumber(double initialValue)
	{
		value = initialValue;
	}

	JsonNumber* JsonNumber::create() const
	{
		return new JsonNumber(0);
	}

	JsonNumber* JsonNumber::clone() const
	{
		JsonNumber* newNumber = new JsonNumber(0);//create new
		*newNumber = *this;//copy "this" to new object
		return newNumber;
	}

	string JsonNumber::serialize(Json_ToStringCharacteristics characteristics) const
	{
		string serialized;

		if (characteristics.pretify)
			serialized += indent(characteristics);

		serialized += std::to_string(value);

		return serialized;
	}

	JsonNumber JsonNumber::parse(string& toParse, JsonStringIndex& currentIndex)
	{
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		// TODO: verify first character

		//---find end of number characters---
		JsonStringIndex endIndex;
		for (endIndex = currentIndex; endIndex < toParse.length(); endIndex++)
		{
			if (!((toParse[endIndex] >= '0' && toParse[endIndex] <= '9') ||
				toParse[endIndex] == '-' ||
				toParse[endIndex] == '.'))// if not number character
			{
				break;//number is finished
			}
		}


		// substring length is defined as the number of times the endIndex was incremented
		try
		{
			string tempSubStr = toParse.substr(currentIndex, endIndex - currentIndex);
			double returnValue = std::stod(tempSubStr);
			currentIndex = endIndex;//we finished processing, so we can set the proper currentIndex
			return JsonNumber(returnValue);
		}
		catch (...)
		{
			throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);
		}
	}

	JSON_TYPE JsonNumber::getType() const
	{
		return JSON_TYPE::NUMBER;
	}

	JsonArray::JsonArray(Array<Json> jsonArray)
	{
		value = jsonArray;// copy
	}

	JsonArray::JsonArray(const JsonArray& other)
	{
		value = other.value;
	}

	JsonArray::JsonArray(JsonArray&& other)
	{
		swap(*this, other);
	}

	JsonArray& JsonArray::operator=(JsonArray other)
	{
		swap(*this, other);
		return *this;
	}

	JsonArray::~JsonArray()
	{

	}

	JsonArray* JsonArray::create() const
	{
		return new JsonArray(Array<Json>());
	}

	JsonArray* JsonArray::clone() const
	{
		JsonArray* newArray = new JsonArray(Array<Json>());//create new
		*newArray = *this;//copy "this" to new object
		return newArray;
	}

	string JsonArray::serialize(Json_ToStringCharacteristics characteristics) const
	{
		string serialized;

		//--start of array---
		if (characteristics.pretify)
			serialized += indent(characteristics) + '[' + '\n';
		else
			serialized += '[';

		//---for each element in the array---
		bool isFirstLoop = true;
		for (Json& element : value)
		{
			if (characteristics.pretify)
			{
				if (!isFirstLoop)
					serialized += ",\n";

				serialized += element.serialize(characteristics.next());
			}
			else
			{
				if (!isFirstLoop)
					serialized += ", ";

				serialized += element.serialize();
			}
			isFirstLoop = false;
		}
		if (characteristics.pretify)
		{
			serialized += "\n";// for last element

			serialized += indent(characteristics);// for end bracket
		}

		//---end of array---
		serialized += "]";

		return serialized;
	}

	JsonArray JsonArray::parse(string& toParse, JsonStringIndex& currentIndex)
	{
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		BasicList<Json> tempArrayStorage;

		if (toParse[currentIndex] != '[')// The array should start with a left bracket
			throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

		currentIndex++;
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		Json::skipWhiteSpace(toParse, currentIndex);// advance to next non-whitespace
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		while (toParse[currentIndex] != ']')
		{
			//---process Json value---//
			tempArrayStorage.enqueue(Json::parse(toParse, currentIndex));
			if (currentIndex >= toParse.length())
				throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

			Json::skipWhiteSpace(toParse, currentIndex);// advance to next non-whitespace
			if (currentIndex >= toParse.length())
				throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

			//---process comma if it exists, else we are done---//
			if (toParse[currentIndex] == ',')
			{
				currentIndex++;
				if (currentIndex >= toParse.length())
					throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

				Json::skipWhiteSpace(toParse, currentIndex);// advance to next non-whitespace
				if (currentIndex >= toParse.length())
					throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);
			}
			else if (toParse[currentIndex] == ']')
			{
				break;
			}
			else
			{
				throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);
			}
		}

		// last character was a right bracket, so we advance one more
		currentIndex++;
		// don't check for bounds since bracket could have been last character

		// we finished parsing, now create the array from the list
		JsonArrayIndex arraySize = tempArrayStorage.size();
		Array<Json> jsonArray(arraySize);

		for (JsonArrayIndex i = 0; i < arraySize; i++)
		{
			jsonArray[i] = tempArrayStorage.pop();
		}

		return JsonArray(jsonArray);
	}

	JSON_TYPE JsonArray::getType() const
	{
		return JSON_TYPE::ARRAY;
	}

	JsonObjectPair::JsonObjectPair()
	{

	}

	JsonObjectPair* JsonObjectPair::create() const
	{
		return new JsonObjectPair();
	}

	JsonObjectPair* JsonObjectPair::clone() const
	{
		JsonObjectPair* newPair = new JsonObjectPair();//create new
		*newPair = *this;//copy "this" to new object
		return newPair;
	}

	string JsonObjectPair::serialize(Json_ToStringCharacteristics characteristics) const
	{
		string serialized;

		if (characteristics.pretify)
			serialized += indent(characteristics);

		serialized += name.serialize() + " : ";// add ["name" : ]

		if (characteristics.pretify)
		{
			serialized += "\n";
		}

		serialized += value.serialize(characteristics.next());

		return serialized;
	}
	
	JsonObjectPair JsonObjectPair::parse(string& toParse, JsonStringIndex& currentIndex)
	{
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		JsonObjectPair newObjectPair;

		//---parse name---//
		if (toParse[currentIndex] != '\"')// the object pair should start with a " character
			throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

		newObjectPair.name = JsonString::parse(toParse, currentIndex);

		Json::skipWhiteSpace(toParse, currentIndex);// advance to next non-whitespace
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		//---parse colon---//
		if (toParse[currentIndex] != ':')
			throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

		currentIndex++;
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		Json::skipWhiteSpace(toParse, currentIndex);// advance to next non-whitespace
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

																		//---parse value---//
		newObjectPair.value = Json::parse(toParse, currentIndex);

		return newObjectPair;
	}

	JSON_TYPE JsonObjectPair::getType() const
	{
		return JSON_TYPE::OBJECT_PAIR;
	}

	JsonObject::JsonObject()
	{
	}

	JsonObject::JsonObject(const JsonObject& other)
	{
		for (JsonObjectPair* pair : other.value)
		{
			JsonObjectPair* newPair = new JsonObjectPair();
			*newPair = *pair;//copy pair from "other" JsonObject
			value.insert(newPair);
		}
	}

	JsonObject::JsonObject(JsonObject&& other)
	{
		swap(*this, other);
	}

	JsonObject& JsonObject::operator=(JsonObject other)
	{
		swap(*this, other);
		return *this;
	}

	JsonObject::~JsonObject()
	{
		for (JsonObjectPair* pair : value/*set*/)
		{
			delete pair;
		}
	}

	JsonObject* JsonObject::create() const
	{
		return new JsonObject();
	}

	JsonObject* JsonObject::clone() const
	{
		JsonObject* newObject = new JsonObject();//create new
		*newObject = *this;//copy "this" to new object
		return newObject;
	}

	string JsonObject::serialize(Json_ToStringCharacteristics characteristics) const
	{
		string serialized;

		// indent for beginning bracket
		if (characteristics.pretify)
			serialized += indent(characteristics);

		serialized += '{';

		if (characteristics.pretify)
			serialized += '\n';
		else
			serialized += ' ';

		size_t setSize = value.size();
		size_t currentPair = 1;// one indexed so "setSize" corresponds to last element
		for (JsonObjectPair* pair : value/*the set*/)
		{
			serialized += pair->serialize(characteristics.next());

			if (currentPair < setSize)// if we aren't on the last element
			{
				serialized += ';';
			}

			if (characteristics.pretify)
				serialized += '\n';
			else
				serialized += ' ';

			currentPair++;
		}

		// indent for ending bracket
		if (characteristics.pretify)
			serialized += indent(characteristics);

		serialized += '}';

		return serialized;
	}

	JsonObject JsonObject::parse(string& toParse, JsonStringIndex& currentIndex)
	{
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		JsonObject newObject;

		if (toParse[currentIndex] != '{')
			throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

		currentIndex++;
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		Json::skipWhiteSpace(toParse, currentIndex);// advance to next non-whitespace
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		while (toParse[currentIndex] != '}')
		{
			JsonObjectPair* newPair = new JsonObjectPair();
			*newPair = JsonObjectPair::parse(toParse, currentIndex);
			newObject.value.insert(newPair);

			Json::skipWhiteSpace(toParse, currentIndex);// advance to next non-whitespace
			if (currentIndex >= toParse.length())
				throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

			if (toParse[currentIndex] == ',')
			{
				currentIndex++;
				if (currentIndex >= toParse.length())
					throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

				Json::skipWhiteSpace(toParse, currentIndex);// advance to next non-whitespace
				if (currentIndex >= toParse.length())
					throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);
			}
		}

		// last character was a right brace, so we advance one more
		currentIndex++;
		// don't check for bounds since brace is last character that we need

		return newObject;
	}

	JSON_TYPE JsonObject::getType() const
	{
		return JSON_TYPE::OBJECT;
	}

	JsonString::JsonString(string initialValue)
	{
		value = initialValue;
	}

	JsonString* JsonString::create() const
	{
		return new JsonString("");
	}

	JsonString* JsonString::clone() const
	{
		JsonString* newString = new JsonString("");//create new
		*newString = *this;//copy "this" to new object
		return newString;
	}

	string JsonString::serialize(Json_ToStringCharacteristics characteristics) const
	{
		string serialized = "";

		if (characteristics.pretify)
			serialized += indent(characteristics);

		serialized += "\"";//beginning of string

		for (size_t i = 0; i < value.length(); i++)
		{
			//these conversions are only valid for ASCII
			if (value[i] == '\"')
			{
				serialized += "\\\""; // (\")
			}
			else if (value[i] == '\\')
			{
				serialized += "\\\\"; // (\\)
			}
			else if (value[i] == '/')
			{
				serialized += "\\/"; // (\/)
			}
			else if (value[i] <= 126 && value[i] >= 32)/*printable characters - escaped characters*/
			{
				serialized += value[i];
			}
			else if (value[i] == 8)/*backspace*/
			{
				serialized += "\\b"; // (\b)
			}
			else if (value[i] == 12)/*form feed*/
			{
				serialized += "\\f";
			}
			else if (value[i] == 10)/*line feed*/
			{
				serialized += "\\n";
			}
			else if (value[i] == 13)/*carriage return*/
			{
				serialized += "\\r";
			}
			else if (value[i] == 9)/*tab*/
			{
				serialized += "\\t";
			}
			else
			{
				serialized += "\\u" + charToCode(value[i]);
			}
		}

		serialized += "\"";//ending of the string

		return serialized;
	}

	JsonString JsonString::parse(string& toParse, JsonStringIndex& currentIndex)
	{
		if (currentIndex >= toParse.length()) 
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		JsonString newString("");

		if (toParse[currentIndex] != '\"')
			throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

		currentIndex++;
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		Json::skipWhiteSpace(toParse, currentIndex);// advance to next non-whitespace
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		while (toParse[currentIndex] != '\"')
		{
			if ((toParse[currentIndex] >= ']' && toParse[currentIndex] <= '~') ||
				(toParse[currentIndex] >= '0' && toParse[currentIndex] <= '[') ||
				(toParse[currentIndex] >= '#' && toParse[currentIndex] <= '.') ||
				(toParse[currentIndex] >= ' ' && toParse[currentIndex] <= '!')
				)//non-escaped characters
			{
				newString.value += toParse[currentIndex];

				currentIndex++;
				if (currentIndex >= toParse.length())
					throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);
			}
			else if (toParse[currentIndex] == '\\')//escaped character
			{
				currentIndex++;//for escape character
				if (currentIndex >= toParse.length())
					throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

				switch (tolower(toParse[currentIndex]))
				{
					case '\"':
					{
						newString.value += "\"";

						currentIndex++;//for escaped character
						if (currentIndex >= toParse.length())
							throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

						break;
					}
					case '\\':
					{
						newString.value += "\\";

						currentIndex++;//for escaped character
						if (currentIndex >= toParse.length())
							throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

						break;
					}
					case '/':
					{
						newString.value += "/";

						currentIndex++;//for escaped character
						if (currentIndex >= toParse.length())
							throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

						break;
					}
					case 'b'://backspace
					{
						newString.value += 8;

						currentIndex++;//for escaped character
						if (currentIndex >= toParse.length())
							throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

						break;
					}
					case 'f'://form feed
					{
						newString.value += 12;

						currentIndex++;//for escaped character
						if (currentIndex >= toParse.length())
							throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

						break;
					}
					case 'n'://line feed
					{
						newString.value += 10;

						currentIndex++;//for escaped character
						if (currentIndex >= toParse.length())
							throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

						break;
					}
					case 'r'://carriage return
					{
						newString.value += 13;

						currentIndex++;//for escaped character
						if (currentIndex >= toParse.length())
							throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

						break;
					}
					case 't'://tab
					{
						newString.value += 9;

						currentIndex++;//for escaped character
						if (currentIndex >= toParse.length())
							throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

						break;
					}
					case 'u'://hex encoded character
					{
						currentIndex += 5;//for the u + next four numbers
						if (currentIndex >= toParse.length())
							throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

						string hex = toParse.substr(currentIndex - 4, 4);
						uint8_t decimal;
						sscanf_s(hex.c_str(), "%hhx", &decimal);
						newString.value += decimal;
						break;
					}
					default:
					{
						throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);
					}
				}
			}
			else if (toParse[currentIndex] == '\"')//end of string
			{
				break;
			}
			else
			{
				throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);
			}
		}

		currentIndex++;//for last " , don't check bounds since we don't need any more characters


		return newString;
	}

	JSON_TYPE JsonString::getType() const
	{
		return JSON_TYPE::STRING;
	}

	string JsonString::charToCode(uint32_t character)
	{
		std::stringstream hexStream;
		hexStream << std::hex << character;
		std::string hexString = hexStream.str();

		return string(4 - hexString.length(), '0') + hexString;
	}

	JsonBool::JsonBool(bool initialValue)
	{
		value = initialValue;
	}

	JsonBool* JsonBool::create() const
	{
		return new JsonBool(false);
	}

	JsonBool* JsonBool::clone() const
	{
		JsonBool* newBool = new JsonBool(false);//create new
		*newBool = *this;//copy "this" to new object
		return newBool;
	}

	string JsonBool::serialize(Json_ToStringCharacteristics characteristics) const
	{
		if (value)//true
		{
			return indent(characteristics) + "true";
		}
		else
		{
			return indent(characteristics) + "false";
		}
	}

	JsonBool JsonBool::parse(string& toParse, JsonStringIndex& currentIndex)
	{
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		if (std::tolower(toParse[currentIndex]) == 't')
		{
			currentIndex++;
			if (currentIndex >= toParse.length())
				throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

			if (std::tolower(toParse[currentIndex]) != 'r')
				throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

			currentIndex++;
			if (currentIndex >= toParse.length())
				throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

			if (std::tolower(toParse[currentIndex]) != 'u') 
				throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

			currentIndex++;
			if (currentIndex >= toParse.length())
				throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

			if (std::tolower(toParse[currentIndex]) != 'e') 
				throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

			currentIndex++;

			return JsonBool(true);
		}
		else if (std::tolower(toParse[currentIndex]) == 'f')
		{
			currentIndex++;
			if (currentIndex >= toParse.length())
				throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

			if (std::tolower(toParse[currentIndex]) != 'a') 
				throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

			currentIndex++;
			if (currentIndex >= toParse.length())
				throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

			if (std::tolower(toParse[currentIndex]) != 'l')
				throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

			currentIndex++;
			if (currentIndex >= toParse.length())
				throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

			if (std::tolower(toParse[currentIndex]) != 's')
				throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

			currentIndex++;
			if (currentIndex >= toParse.length())
				throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

			if (std::tolower(toParse[currentIndex]) != 'e') 
				throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

			currentIndex++;

			return JsonBool(false);
		}
		else
		{
			throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);
		}
	}

	JSON_TYPE JsonBool::getType() const
	{
		return JSON_TYPE::BOOL;
	}

	JsonInternalValue::~JsonInternalValue()
	{
	}

	JsonInternalValue* JsonInternalValue::parse(string toParse, JsonStringIndex& currentIndex)
	{
		Json::skipWhiteSpace(toParse, currentIndex);

		if (toParse[currentIndex] == '[')							// array
		{
			return JsonArray::parse(toParse, currentIndex).clone();
		}
		else if (toParse[currentIndex] == '{')						// object
		{
			return JsonObject::parse(toParse, currentIndex).clone();
		}
		else if (toParse[currentIndex] == '\"')						// string
		{
			return JsonString::parse(toParse, currentIndex).clone();
		}
		else if (toParse[currentIndex] >= '0' &&
			toParse[currentIndex] <= '9' ||
			toParse[currentIndex] == '-')							// number
		{
			return JsonNumber::parse(toParse, currentIndex).clone();
		}
		else
		{
			if (std::tolower(toParse[currentIndex]) == 'n')			// null
			{
				return JsonNull::parse(toParse, currentIndex).clone();
			}
			else if (std::tolower(toParse[currentIndex]) == 't')	// bool
			{
				return JsonBool::parse(toParse, currentIndex).clone();
			}
			else if (std::tolower(toParse[currentIndex]) == 'f')	// bool
			{
				return JsonBool::parse(toParse, currentIndex).clone();
			}
			else
			{
				throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);
			}
		}
	}

	JsonNull::JsonNull()
	{
	}

	JsonNull* JsonNull::create() const
	{
		return new JsonNull();
	}

	JsonNull* JsonNull::clone() const
	{
		return new JsonNull();//there is no difference between JsonNull objects
	}

	string JsonNull::serialize(Json_ToStringCharacteristics characteristics) const
	{
		return indent(characteristics) + "null";
	}

	JsonNull JsonNull::parse(string& toParse, JsonStringIndex& currentIndex)
	{
		if (currentIndex >= toParse.length())
			throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

		if (std::tolower(toParse[currentIndex]) == 'n')
		{
			currentIndex++;
			if (currentIndex >= toParse.length())
				throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

			if (std::tolower(toParse[currentIndex]) != 'u') 
				throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

			currentIndex++;
			if (currentIndex >= toParse.length())
				throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

			if (std::tolower(toParse[currentIndex]) != 'l')
				throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

			currentIndex++;
			if (currentIndex >= toParse.length())
				throw Json_ParseStringOverrunException(__func__, __LINE__, __FILE__);

			if (std::tolower(toParse[currentIndex]) != 'l')
				throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);

			currentIndex++;

			return JsonNull();
		}
		else
		{
			throw Json_ParseStringInvalidFormatException(currentIndex, __func__, __LINE__, __FILE__);
		}
	}

	JSON_TYPE JsonNull::getType() const
	{
		return JSON_TYPE::NIL;
	}

	void swap(Json& first, Json& second)
	{
		using std::swap;

		swap(first.valuePtr, second.valuePtr);
	}

	void swap(JsonArray& first, JsonArray& second)
	{
		using std::swap;

		swap(first.value, second.value);
	}

	void swap(JsonObject& first, JsonObject& second)
	{
		using std::swap;

		swap(first.value, second.value);
	}

	string indent(Json_ToStringCharacteristics characteristics)
	{
		return string(characteristics.indentCount, characteristics.indentCharacter);
	}

}// end of BrendanLib