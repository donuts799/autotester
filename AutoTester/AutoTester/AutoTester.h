#pragma once
#include <list>
#include <mutex>
#include <condition_variable>
#include <map>
#include <fstream>
//#include <Array>
#include "ProcessProcessor.h"


//TODO: timeout, threadcount, ???
//TODO: safe threadCount decrement (RAII)

class ScriptInfo
{
public:
	std::string	scriptLocation;
	std::string	scriptName;
	time_t		timeout;
	std::string script;
};

class AutoTesterSettings
{
public:
	uint64_t	MaxThreadCount;
	bool		scriptsMultithreaded;

};

class AutoTester
{
public:
	AutoTester(std::list<std::string> projects, std::list<ScriptInfo> scripts, AutoTesterSettings settings);
	~AutoTester();

	void run();
	void waitUntilFinished();

	enum class ThreadCount : uint32_t
	{
		ALL_THE_THREADS = UINT32_MAX,
		DEFAULT			= 1
	};
private:
	std::list<std::string>	projectList;
	std::list<ScriptInfo>	scripts;
	volatile uint32_t allocatedThreadCount		= 0;
	AutoTesterSettings settings;
	bool testerRunning = false;

	std::mutex conditionMutex;
	std::condition_variable condition;

	void runProject(std::string		projectName);
	void runScript(std::string		projectName, ScriptInfo scriptName);
	void runScripts(std::string		projectName); // already has access to the script map, so no need to pass it
	void timeScript(std::string		projectName, ScriptInfo scriptName);
};

