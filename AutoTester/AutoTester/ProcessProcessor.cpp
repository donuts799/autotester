#include <windows.h>
//#include <atlstr.h>
//#include <afx.h>
#include <CString>
#include <string>
#include "ProcessProcessor.h"

int ReadFromPipeNoWait(HANDLE hPipe, char* pDest, int nMax)
{
	DWORD nBytesRead = 0;
	DWORD nAvailBytes;
	char cTmp;
	memset(pDest, 0, nMax);
	// -- check for something in the pipe
	PeekNamedPipe(hPipe, &cTmp, 1, NULL, &nAvailBytes, NULL);
	if (nAvailBytes == 0) {
		return(nBytesRead);
	}
	// OK, something there... read it
	ReadFile(hPipe, pDest, nMax - 1, &nBytesRead, NULL);
	return(nBytesRead);
}

std::string ReadFromPipeNoWait(HANDLE hPipe)
{
	DWORD nBytesRead = 0;
	DWORD nAvailBytes;
	char* pDest;
	char cTmp;
	// -- check for something in the pipe
	PeekNamedPipe(hPipe, &cTmp, 1, NULL, &nAvailBytes, NULL);
	if (nAvailBytes == 0) {
		return "";
	}
	// OK, something there... read it
	pDest = new char[nAvailBytes + 1];
	memset(pDest, 0, nAvailBytes + 1);

	ReadFile(hPipe, pDest, nAvailBytes, &nBytesRead, NULL);
	std::string result = std::string(pDest);
	delete pDest;
	return result;
}

std::string ExecAndProcessOutput(std::string szCmd, std::string szParams, time_t timeout)// executable, script
{
	std::string result;

	SECURITY_ATTRIBUTES rSA = { 0 };
	rSA.nLength = sizeof(SECURITY_ATTRIBUTES);
	rSA.bInheritHandle = TRUE;

	HANDLE hReadPipe, hWritePipe;
	CreatePipe(&hReadPipe, &hWritePipe, &rSA, 25000);

	PROCESS_INFORMATION rPI = { 0 };
	STARTUPINFOA        rSI = { 0 };
	rSI.cb = sizeof(rSI);
	rSI.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
	rSI.wShowWindow = SW_HIDE;  // or SW_SHOWNORMAL or SW_MINIMIZE
	rSI.hStdOutput = hWritePipe;
	rSI.hStdError = hWritePipe;

	//CString sCmd; sCmd.Format("\"%s\" %s", (LPCSTR)szCmd, (LPCSTR)szParams);
	std::string sCmd; 
	sCmd = szCmd + szParams;

	BOOL fRet = CreateProcessA(NULL, (LPSTR)(LPCSTR)sCmd.c_str(), NULL,
		NULL, TRUE, 0, 0, 0, &rSI, &rPI);
	if (!fRet) {
		throw std::exception("Error while creating process!");
	}
	//------------------------- and process its stdout every 100 ms
	//char dest[1000];
	//CString sProgress = "";
	DWORD dwRetFromWait = WAIT_TIMEOUT;
	while (dwRetFromWait != WAIT_OBJECT_0) {
		dwRetFromWait = WaitForSingleObject(rPI.hProcess, 100);
		if (dwRetFromWait == WAIT_ABANDONED) {  // crash?
			break;
		}
		//--- else (WAIT_OBJECT_0 or WAIT_TIMEOUT) process the pipe data
		//while (ReadFromPipeNoWait(hReadPipe, dest, sizeof(dest)) > 0) {
		while (true)
		{
			std::string output = ReadFromPipeNoWait(hReadPipe);
			if (output == "") break;
			result += output;
		}
	}
	CloseHandle(hReadPipe);
	CloseHandle(hWritePipe);
	CloseHandle(rPI.hThread);
	CloseHandle(rPI.hProcess);
	return result;
}