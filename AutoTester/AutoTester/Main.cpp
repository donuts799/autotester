// AutoTester.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <string>
#include "AutoTester.h"
#include "json.h"

//TODO: Should I use commandline parameters, or just settings files?
using namespace BrendanLib;

void run();

void importProjects(std::ifstream& projectsFile, std::list<std::string>& projects);

void importScripts(std::ifstream& scriptsFile, std::list<ScriptInfo>& scripts);

void importSettings(std::ifstream& settingsFile, AutoTesterSettings& settings);

int main()
{
	try
	{
		run();
	}
	catch (std::exception& ex)
	{
		std::cerr << ex.what() << std::endl;
		return -1;
	}
	catch (...)
	{
		std::cerr << "Unknown Error" << std::endl;
		return -2;
	}
}

void run()
{
	std::list<std::string> projects;
	std::list<ScriptInfo> scripts;
	AutoTesterSettings settings;

	std::ifstream projectsFile	= std::ifstream("projects.json");
	std::ifstream scriptsFile	= std::ifstream("scripts.json");
	std::ifstream settingsFile	= std::ifstream("settings.json");

	if (projectsFile)
	{
		importProjects(projectsFile, projects);
	}
	else
		throw std::exception("Could not open projects file!");

	if (scriptsFile)
	{
		importScripts(scriptsFile, scripts);
	}
	else
		throw std::exception("Could not open scripts file!");

	if (settingsFile)
	{
		importSettings(settingsFile, settings);
	}
	else
		throw std::exception("Could not open settings file!");


	AutoTester tester(projects, scripts, settings);

	tester.run();

	tester.waitUntilFinished();
}

void importProjects(std::ifstream& projectsFile, std::list<std::string>& projects)
{
	std::string jsonString((std::istreambuf_iterator<char>(projectsFile)), std::istreambuf_iterator<char>());
	Json projectsJson = Json::parse(jsonString);

	for (size_t i = 0; i < projectsJson.size(); i++)
	{
		projects.push_back(projectsJson[i]);
	}
}

void importScripts(std::ifstream& scriptsFile, std::list<ScriptInfo>& scripts)
{
	std::string jsonString((std::istreambuf_iterator<char>(scriptsFile)), std::istreambuf_iterator<char>());
	Json projectsJson = Json::parse(jsonString);

	for (size_t i = 0; i < projectsJson.size(); i++)
	{
		ScriptInfo info;
		info.scriptLocation = (std::string)	projectsJson[i][(std::string)"scriptLocation"];
		info.scriptName		= (std::string)	projectsJson[i][(std::string)"scriptName"];
		info.timeout		= (int)(double)	projectsJson[i][(std::string)"timeout"];

		std::ifstream scriptFile = std::ifstream(info.scriptLocation);
		if (scriptFile)
		{
			info.script = std::string((std::istreambuf_iterator<char>(scriptFile)), std::istreambuf_iterator<char>());
		}
		else
			throw std::exception((std::string("") + "Could not open script: " + info.scriptName).c_str());

		scripts.push_back(info);
	}
}

void importSettings(std::ifstream& settingsFile, AutoTesterSettings& settings)
{
	std::string jsonString((std::istreambuf_iterator<char>(settingsFile)), std::istreambuf_iterator<char>());
	Json settingsJson = Json::parse(jsonString);

	settings.MaxThreadCount			= (double)	settingsJson[(std::string)"maxThreadCount"];
	settings.scriptsMultithreaded	= (bool)	settingsJson[(std::string)"scriptsMultithreaded"];
}
