#include "pch.h"
#include "Process.h"
#include <iostream>
#include <windows.h>



std::string ReadFromPipeNoWait(HANDLE hPipe)
{
	DWORD nBytesRead = 0;
	DWORD nAvailBytes = 0;
	char* pDest;
	char cTmp;
	// -- check for something in the pipe
	PeekNamedPipe(hPipe, &cTmp, 1, NULL, &nAvailBytes, NULL);
	if (nAvailBytes == 0) {
		return "";
	}
	// OK, something there... read it
	pDest = new char[nAvailBytes + 1];
	memset(pDest, 0, nAvailBytes + 1);

	ReadFile(hPipe, pDest, nAvailBytes, &nBytesRead, NULL);
	std::string result = std::string(pDest);
	delete pDest;
	return result;
}

std::string ExecAndProcessOutput(std::string szCmd, std::string szParams)// executable, script
{
	std::string result;

	SECURITY_ATTRIBUTES rSA = { 0 };
	rSA.nLength = sizeof(SECURITY_ATTRIBUTES);
	rSA.bInheritHandle = TRUE;

	HANDLE hReadPipe, hWritePipe;
	CreatePipe(&hReadPipe, &hWritePipe, &rSA, 25000);

	PROCESS_INFORMATION rPI = { 0 };
	STARTUPINFOA        rSI = { 0 };

	rSI.cb = sizeof(rSI);
	rSI.lpTitle = (LPSTR)"SubProject";
	rSI.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
	rSI.wShowWindow = SW_SHOWNORMAL;// SW_HIDE or SW_SHOWNORMAL or SW_MINIMIZE
	rSI.hStdOutput = hWritePipe;
	rSI.hStdError = hWritePipe;

	std::string sCmd = szCmd;

	BOOL fRet = 
		CreateProcessA(
			NULL,
			(LPSTR)(LPCSTR)sCmd.c_str(),
			NULL,						// Inherit Process Handle
			NULL,						// Inherit Thread Handle
			TRUE,						// Inherit from current process
			CREATE_NEW_CONSOLE,			// Console Creation Flags
			0,							// Environment Block
			0,							// Starting Directory
			&rSI,						// Startup Info
			&rPI						// Process Info
		);

	if (!fRet) {
		throw std::exception("Error while creating process!");
	}
	//------------------------- and process its stdout every 100 ms
	//CString sProgress = "";
	DWORD dwRetFromWait = WAIT_TIMEOUT;
	while (dwRetFromWait != WAIT_OBJECT_0) {
		dwRetFromWait = WaitForSingleObject(rPI.hProcess, 100);
		if (dwRetFromWait == WAIT_ABANDONED) {  // crash?
			break;
		}
		//--- else (WAIT_OBJECT_0 or WAIT_TIMEOUT) process the pipe data
		//while (ReadFromPipeNoWait(hReadPipe, dest, sizeof(dest)) > 0) {
		while (true)
		{
			std::string output = ReadFromPipeNoWait(hReadPipe);
			if (output == "") break;
			std::cout << output;
		}
	}
	CloseHandle(hReadPipe);
	CloseHandle(hWritePipe);
	CloseHandle(rPI.hThread);
	CloseHandle(rPI.hProcess);
	return result;
}